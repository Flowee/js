/*
 * This file is part of the Flowee project
 * Copyright (C) 2019-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEE_H
#define FLOWEE_H

#include <napi.h>
#include <Blockchain.h>

namespace Flowee {
    struct Callback {
        Callback(Napi::ThreadSafeFunction && function)
            : f(std::move(function)){}
        Callback() {}
        ~Callback() { free(); }
        inline bool acquire() {
            if (!acquired)
                acquired = f.Acquire() == napi_ok;
            return acquired;
        }
        void free() {
            if (acquired) {
                f.Release();
                acquired = false;
            }
        }

        bool acquired = false;
        Napi::ThreadSafeFunction f;
    };
    struct PromiseCallback {
        PromiseCallback(Napi::Env env);

        void resolve(Napi::Value value);
        Napi::Promise promise(Napi::Env env);
        inline void resolve(Napi::Env env, const std::string &string) {
            resolve(Napi::String::New(env, string));
        }
        void reject(Napi::Value value);
        inline void reject(Napi::Env env, const std::string &string) {
            reject(Napi::String::New(env, string));
        }

        Napi::Promise::Deferred p;
        bool present = false;
    };

    Napi::Object populateTransaction(Napi::Env env, const Blockchain::Transaction &transaction, bool binaryHash);

    Napi::Uint8Array wrap(Napi::Env env, const Streaming::ConstBuffer &buffer);

    Napi::String hashToString(Napi::Env env, const Streaming::ConstBuffer &buffer);


    // return a pool for the current thread;
    Streaming::BufferPool &globalPool(int reserveSize);

    /*
     * Create a buffer of the raw data we get from a hex-string.
     * /param hash string of hex characters (not case sensitive).
     */
    Streaming::ConstBuffer hexStringToBuffer(const std::string &hash);

    constexpr const char *CashAddressMainnet = "bitcoincash";
    constexpr const char *CashAddressTestnet = "bchtest";
    /*
     * Create a sha256 hash from the user input.
     */
    Streaming::ConstBuffer parseAddress(const Napi::String &string, std::string *prefix = nullptr, bool *isHash = nullptr);
}

#endif
