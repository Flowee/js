FloweeJS is a binding module enabling JavaScript developers to use the
Flowee applications and libraries with the NodeJS JavaScript engine.

Flowee is a family of products and our goal is to move the world towards
a Bitcoin Cash economy. FloweeJS works together with various other Flowee
products for the fastest and simplest way to build Bitcoin Cash products.

Using the online APIs, more people can get started with FloweeJS fast and
for free. Afraid of lock-in? No worry, the Flowee stack can be run on your
own server or on some cloud service, with ease. And again, free of charge.


The APIs are documented on [flowee.org/docs/floweejs](https://flowee.org/docs/floweejs)
and you can find our examples repo
[here](https://gitlab.com/FloweeTheHub/floweejs-examples/).


## Install

FloweeJS uses the nodejs bindings project in order to ensure that changes
in nodejs versions will be irrelevant. No recompiles needed.
This does mean that we have a minimum version of NodeJS (10.16 or later).
We always test on the latest version. Please report your experiences on
different versions!

To install the bindings you need to have available a C++ compiler,
the boost libs and the cmake tool.


For Linux this means a simple;
```
apt-get install libboost-all-dev cmake
npm install floweejs
```

## Contributions

If you want to help out make this the best Javascript Bitcoin experience, you can find
all code, issues and website content on gitlab.

Questions and suggestions go here:
[https://gitlab.com/FloweeTheHub/issues](https://gitlab.com/FloweeTheHub/issues)

The Flowee main website is at: [Flowee.org](https://flowee.org)

Have fun!
